package com.example.demo.Persons;

import lombok.Value;

@Value
public class CreatePerson {
    String namn;
    int alder;
}
