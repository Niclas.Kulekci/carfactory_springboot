package com.example.demo.Persons;


import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder(toBuilder = true)
public class Person {
    String id;
    String namn;
    int alder;
    List<PersonBil> bilar;


    @Value
    static class PersonBil {
        String id;
        String brand;
        String model;
    }
}
