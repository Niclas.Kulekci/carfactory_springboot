package com.example.demo.Persons;

import com.example.demo.bil.BilRepository;
import com.example.demo.bil.BilService;
import com.example.demo.tokens.Token;
import com.example.demo.tokens.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/persons")
@AllArgsConstructor
public class PersonController {

    PersonService personService;
    TokenService tokenService;

    @GetMapping("/token")
    public Token createNewToken(){
        return tokenService.createNewToken();
    }

    @GetMapping("/persons")
    public List<Person> all(@RequestHeader(value = "token", required = false) String tokenString){
        Token token = tokenService.getTokenByTokenString(tokenString);
        return personService.all().map(this::toPerson)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    public Person get(@PathVariable String id){
        return personService.getPersonById(id)
                .map(this::toPerson)
                .orElse(null);
    }

    @GetMapping("/person")
    public Person getTokenPerson( @RequestHeader(value = "token", required = false) String tokenString){
        Token token = tokenService.getTokenByTokenString(tokenString);
        return personService.getPersonById(token.getPersonId())
                .map(this::toPerson)
                .orElse(null);
    }

    @PutMapping()
    public Person create(@RequestBody CreatePerson createPerson,
                         @RequestHeader(value = "token", required = false) String tokenString){
        Token token = tokenService.getTokenByTokenString(tokenString);
        return personService.create(createPerson)
                .map(personEntity ->{
                    if(token != null)
                        token.setPersonId(personEntity.getId());
                    return personEntity;
                })
                .map(this::toPerson)
                .orElse(null);
    }

    @PutMapping("/{id}/nybil")
    public Person create(@PathVariable String id,@RequestParam(value = "brand") String brand,@RequestParam(value = "model") String model){
        return personService.addNewBil(id,brand,model)
                .map(this::toPerson)
                .orElse(null);
    }

    @PostMapping("/{id}")
    public Person update(@RequestBody UpdatePerson updatePerson,@PathVariable("id") String id){
        return personService.update(id,updatePerson)
                .map(this::toPerson)
                .orElse(null);
    }

    @PostMapping("/{id}/changeField/{fieldName}")
    public Person changeField
            (@RequestBody String value,
             @PathVariable("id") String id,
             @PathVariable("fieldName") String fieldName) {
        return personService.changeField(id,PersonFieldName.as(fieldName),value)
                .map(this::toPerson)
                .orElse(null);
    }

    private Person toPerson(PersonEntity personEntity){
        return new Person(personEntity.getId(),
                personEntity.getNamn(),
                personEntity.getAlder(),
                personEntity.bilar.stream()
                        .map(bil -> new Person.PersonBil(bil.getId(),bil.getBrand(),bil.getModel()))
                        .collect(Collectors.toList())
        );
    }


}
