package com.example.demo.Persons;

import com.example.demo.bil.BilEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "person")
public class PersonEntity {
    @Id String id;
    String namn;
    int alder;
    @OneToMany(mappedBy = "person")
    List<BilEntity> bilar;

    public void addBil(BilEntity bilEntity) {
        bilar.add(bilEntity);
    }

    public void deleteBil(BilEntity bilEntity){
        bilar.remove(bilEntity);
    }
}
