package com.example.demo.Persons;

public enum PersonFieldName {
    NAMN,
    ALDER;

    public static PersonFieldName as(String fieldName){
        return PersonFieldName.valueOf(fieldName.toUpperCase());
    }
}
