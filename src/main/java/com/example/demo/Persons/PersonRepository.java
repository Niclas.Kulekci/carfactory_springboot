package com.example.demo.Persons;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, String> {

//    Map<String, PersonEntity> persons = new HashMap<>();

}
