package com.example.demo.Persons;

import com.example.demo.bil.BilEntity;
import com.example.demo.bil.BilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class PersonService {
    PersonRepository personRepository;
    BilRepository bilRepository;

    public PersonService(PersonRepository personRepository, BilRepository bilRepository){
        this.personRepository = personRepository;
        this.bilRepository = bilRepository;
    }


    Stream<PersonEntity> all(){
        return StreamSupport.stream(personRepository.findAll().spliterator(),false);
    }
    public Stream<PersonEntity> all(String search){
        Stream<PersonEntity> personsStream = all();
        return personsStream;
    }

    public Optional<PersonEntity> getPersonById(String id) {
        return personRepository.findById(id);
    }

    public Optional<PersonEntity>  create(CreatePerson createPerson) {
        PersonEntity person = new PersonEntity(
                UUID.randomUUID().toString(),
                createPerson.getNamn(),
                createPerson.getAlder(),
                new ArrayList<>()
        );
        personRepository.save(person);
        return Optional.of(person);
    }

    public Optional<PersonEntity> update(String id, UpdatePerson updatePerson) {
        return getPersonById(id)
                .map(person -> {
                    if( updatePerson.getNamn() != null)
                        person.setNamn(updatePerson.getNamn());
                    if( updatePerson.getAlder() != null)
                        person.setAlder(updatePerson.getAlder());
                    personRepository.save(person);
                    return person;
                });
    }

    public Optional<PersonEntity> changeField(String id, PersonFieldName fieldName, String value) {
        return getPersonById(id)
                .map(person -> {
                    switch (fieldName){
                        case NAMN: person.setNamn(value); break;
                        case ALDER: person.setAlder(Integer.parseInt(value)); break;
                    }
                    personRepository.save(person);
                    return person;
                });
    }

    public Optional<PersonEntity> addNewBil(String id, String brand, String model) {
        return getPersonById(id)
                .map(person -> {
                    BilEntity bilEntity = new BilEntity(UUID.randomUUID().toString(),person,brand,model);
                    person.addBil(bilEntity);
                    bilRepository.save(bilEntity);
                    personRepository.save(person);
                    return person;
                });
    }
}
