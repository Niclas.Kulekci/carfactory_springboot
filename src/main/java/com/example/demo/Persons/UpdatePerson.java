package com.example.demo.Persons;

import lombok.Value;

@Value
public class UpdatePerson {
    String namn;
    Integer alder;
}
