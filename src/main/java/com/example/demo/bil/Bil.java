package com.example.demo.bil;


import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class Bil {
    String id;
    String personId;
    String personName;
    String brand;
    String model;
}
