package com.example.demo.bil;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/bilar")
@AllArgsConstructor
public class BilController {

    BilService bilService;

    @GetMapping("/")
    public List<Bil> all(){
        return bilService.all()
                .map(BilController::toBil)
                .collect(Collectors.toList());
    }

    private static Bil toBil (BilEntity bilEntity){
        return new Bil(bilEntity.getId(),
                bilEntity.getPerson().getId(),
                bilEntity.person.getNamn(),
                bilEntity.getBrand(),
                bilEntity.getModel());
    }

//    @DeleteMapping("{id}")
//    public void deleteBil(@PathVariable String id){
//        bilService.deleteByBilId(id);
//    }
}
