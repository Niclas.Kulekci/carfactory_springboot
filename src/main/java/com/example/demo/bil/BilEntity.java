package com.example.demo.bil;

import com.example.demo.Persons.PersonEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "bil")
public class BilEntity {
    @Id String id;
    @ManyToOne
    @JoinColumn(name = "person", nullable = false)
    PersonEntity person;
    String brand;
    String model;

}
