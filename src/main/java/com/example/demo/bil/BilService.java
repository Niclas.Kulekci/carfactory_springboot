package com.example.demo.bil;

import com.example.demo.Persons.PersonEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class BilService {

    BilRepository bilRepository;

    public Stream<BilEntity> all() {
        return bilRepository.findAll().stream();
    }

//    public void deleteByBilId(String id) {
//        BilEntity bilEntity = bilRepository.get(id);
//        PersonEntity personEntity = bilEntity.getPerson();
//        personEntity.deleteBil(bilEntity);
//        bilRepository.remove(id);
//    }
}
