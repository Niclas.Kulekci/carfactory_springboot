package com.example.demo.tokens;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Token {
    String id;
    String personId;
}
