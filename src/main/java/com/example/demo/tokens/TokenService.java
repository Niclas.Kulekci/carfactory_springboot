package com.example.demo.tokens;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class TokenService {

    Map<String,Token> tokenMap = new HashMap<>();


    public Token createNewToken(){
        Token token = new Token(UUID.randomUUID().toString(), null);
        tokenMap.put(token.getId(),token);
        return token;
    }

    public Token getTokenByTokenString(String tokenString){
        return tokenMap.get(tokenString);
    }
}
